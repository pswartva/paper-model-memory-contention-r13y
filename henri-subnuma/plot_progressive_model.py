#!/usr/bin/env python3

import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import statistics

from memory_contention import *


comp_kernel = "memset_nt"
hwloc_file = "topology.xml"
numa_comp = 0
numa_comm = 0
op = "memset"
MARKER_SIZE_PAR = 7
MARKER_PAR = "v"
error_total_model_x_start = 7  # number of cores when memory bus capacity is reached - 1


print("** Parsing Hwloc topology...")

topo = HwlocTopology(hwloc_file, levels_to_remove=["L3Cache"])

print(f"{topo.nb_sockets} sockets")
print(f"{topo.nb_numa_nodes_total} NUMA nodes")
print(f"{topo.nb_cores_total} cores")

nb_cores_to_consider = topo.nb_cores_per_numa_node * topo.nb_numa_nodes_per_socket
if topo.nb_numa_nodes_total == 1:
    nb_cores_to_consider -= 1  # one core is used by communication thread
print(f"Will consider the performance with up to {nb_cores_to_consider} cores")

x_span_zone = topo.nb_cores_per_numa_node if topo.nb_numa_nodes_per_socket > 1 else None

all_files = [
    f"{comp_kernel}/comp_{numa_comp}_comm_{numa_comm}_{i}_threads.out"
    for i in range(1, nb_cores_to_consider+1)
]

parser = FilesParser(all_files)
real_x_values = parser.x_values[:nb_cores_to_consider]
metric = parser.compute_bench_type.default_metric

colors = iter(plt.rcParams["axes.prop_cycle"].by_key()["color"])
color_comp = next(colors)
color_comm = next(colors)
color_comp_alone = next(colors)
color_total = next(colors)
_ = next(colors)
_ = next(colors)
_ = next(colors)
_ = next(colors)
color_comp_model = next(colors)
color_comm_model = next(colors)

plot_title = f"Comp. data on NUMA node {numa_comp} - Comm. data on NUMA node {numa_comm}"


def compute_error(observation, model):
    return statistics.mean(map(lambda o, m: abs(o-m)/o, observation, model))


def plot_repartition(picture_file_suffix, comm_with_comp_model, comp_with_comm_model, model_name):
    fig_stack = plt.figure()
    ax = fig_stack.gca()
    plt.stackplot(
        real_x_values,
        [
            [parser.comp_with_comm_results[op][x][metric]['avg'] for x in real_x_values],
            [parser.comm_bw_with_comp_results[x]['med'] for x in real_x_values]
        ],
        labels=["Computations (with communications)", "Communications (with computations)"],
        colors=[color_comm, color_comp],
    )
    plt.plot(
        real_x_values,
        comp_with_comm_model,
        "--",
        color=color_comp_model,
        label="Computations (with communications) - model " + model_name
    )
    plt.plot(
        real_x_values,
        [sum(x) for x in zip(comm_with_comp_model, comp_with_comm_model)],
        "--",
        color=color_comm_model,
        label="Communications (with computations) - model " + model_name
    )
    handles, labels = ax.get_legend_handles_labels()
    legend_order = [2, 3, 0, 1]
    ax.legend(
        [handles[i] for i in legend_order],
        [labels[i] for i in legend_order],
        loc="upper center",
        bbox_to_anchor=(0.485, -0.15),
        ncol=2,
        fontsize=9
    )
    ax.grid(axis='y')
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.yaxis.set_major_formatter(CommCompGraph.mega2giga_formatter)
    ax.set_axisbelow(True)
    ax.set(title=plot_title, xlabel="Number of computing Cores", ylabel="Memory Bandwidth (GB/s)")
    plt.subplots_adjust(bottom=0.22)
    plt.savefig(f"{comp_kernel}/comp_{numa_comp}_comm_{numa_comm}_stacked{picture_file_suffix}", bbox_inches="tight")
    plt.close(fig_stack)


    graph = CommCompGraph(
        real_x_values,
        parser.x_type,
        CommCompGraphCommType.BANDWIDTH,
        parser.compute_bench_type,
        parser.compute_bench_type.default_metric,
        title=plot_title,
        comp_legend_title=f"Computation Bandwidth - {comp_kernel}",
        comm_legend_title="Network Bandwidth",
        legend_fontsize=9.5,
        x_span_zone=x_span_zone,
        giga_bw=True,
        legend_position="lower",
    )

    comm_with_comp_values = [parser.comm_bw_with_comp_results[x]['med'] for x in real_x_values]
    comm_with_comp_upper_values = [parser.comm_bw_with_comp_results[x]['d9'] for x in real_x_values]

    graph.add_comm_curve(
        comm_with_comp_values,
        "Measure (with Computations)",
        CommCompGraphCurveType.PARALLEL,
        True,
        [parser.comm_bw_with_comp_results[x]['d1'] for x in real_x_values],
        comm_with_comp_upper_values,
        display_line=False,
        markersize=MARKER_SIZE_PAR,
        marker=MARKER_PAR
    )

    graph.add_comm_curve(
        comm_with_comp_model,
        f"Model {model_name} (with Computations)",
        CommCompGraphCurveType.ALONE,
        True,
        display_line=True
    )

    comp_with_comm_values = [parser.comp_with_comm_results[op][x][metric]['avg'] for x in real_x_values]
    graph.add_comp_curve(
        comp_with_comm_values,
        "Measure (with Communications)",
        CommCompGraphCurveType.PARALLEL,
        False,
        [parser.comp_with_comm_results[op][x][metric]['min'] for x in real_x_values],
        [parser.comp_with_comm_results[op][x][metric]['max'] for x in real_x_values],
        display_line=False,
        markersize=MARKER_SIZE_PAR,
        marker=MARKER_PAR
    )

    graph.add_comp_curve(
        comp_with_comm_model,
        f"Model {model_name} (with Communications)",
        CommCompGraphCurveType.ALONE,
        True,
        display_line=True
    )
    graph.save(f"{comp_kernel}/comp_{numa_comp}_comm_{numa_comm}{picture_file_suffix}")
    graph.close()

    error_comm = compute_error(comm_with_comp_values, comm_with_comp_model)
    error_comp = compute_error(comp_with_comm_values, comp_with_comm_model)
    print(f"Error on communication prediction: {error_comm*100:3.2f}%")
    print(f"Error on computation prediction: {error_comp*100:3.2f}%")



print("#### Plot 1 (total is constant)")

picture_file_suffix = "_model_v1.png"

model = CommCompModel(all_files)

fig_stack = plt.figure()
ax = fig_stack.gca()
plt.stackplot(
    real_x_values,
    [[parser.comp_with_comm_results[op][x][metric]['avg'] for x in real_x_values],
    [parser.comm_bw_with_comp_results[x]['med'] for x in real_x_values]],
    labels=["Computations (with communications)", "Communications (with computations)"],
    colors=[color_comm, color_comp],
)
plt.plot(
    real_x_values,
    [parser.comp_alone_results[op][x][metric]['avg'] for x in real_x_values],
    color=color_comp_alone,
    label="Computations (alone)"
)
plt.plot(
    real_x_values,
    [model.max_together_value]*len(real_x_values),
    "--",
    color=color_total,
    label="Bus capacity - model CONSTANT"
)
handles, labels = ax.get_legend_handles_labels()
legend_order = [2, 3, 0, 1]
ax.legend(
    [handles[i] for i in legend_order],
    [labels[i] for i in legend_order],
    loc="upper center",
    bbox_to_anchor=(0.5, -0.15),
    ncol=2,
    fontsize=10
)
ax.grid(axis='y')
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
ax.yaxis.set_major_formatter(CommCompGraph.mega2giga_formatter)
ax.set_axisbelow(True)
ax.set(title=plot_title, xlabel="Number of computing Cores", ylabel="Memory Bandwidth (GB/s)")
plt.subplots_adjust(bottom=0.22)
plt.savefig(f"{comp_kernel}/comp_{numa_comp}_comm_{numa_comm}_stacked{picture_file_suffix}", bbox_inches="tight")
plt.close(fig_stack)

error = compute_error(
    [parser.comp_with_comm_results[op][x][metric]['avg']+parser.comm_bw_with_comp_results[x]['med'] for x in real_x_values[error_total_model_x_start:]],
    [model.max_together_value]*(len(real_x_values)-error_total_model_x_start),
)
print(f"Error on total prediction: {error*100:3.2f}%")



print("#### Plot 2 (total is linear after contention)")

picture_file_suffix = "_model_v2.png"

total_model = []
degradation_per_core = (model.total_parallel[model.max_together_i] - model.total_parallel[-1]) / (model.xs[-1] + 1 - model.xs[model.max_together_i])
for i in range(len(model.x_model)):
    if model.x_model[i] <= model.xs[model.max_together_i]:
        total_model.append(model.max_together_value)
    else:
        total_model.append(model.max_together_value - (degradation_per_core * (model.x_model[i]-model.xs[model.max_together_i])))

fig_stack = plt.figure()
ax = fig_stack.gca()
plt.stackplot(
    real_x_values,
    [
        [parser.comp_with_comm_results[op][x][metric]['avg'] for x in real_x_values],
        [parser.comm_bw_with_comp_results[x]['med'] for x in real_x_values],
    ],
    labels=["Computations (with communications)", "Communications (with computations)"],
    colors=[color_comm, color_comp],
)
plt.plot(
    real_x_values,
    [parser.comp_alone_results[op][x][metric]['avg'] for x in real_x_values],
    color=color_comp_alone,
    label="Computations (alone)"
)
plt.plot(
    real_x_values,
    total_model,
    "--",
    color=color_total,
    label="Bus capacity - model LINEAR1"
)
handles, labels = ax.get_legend_handles_labels()
legend_order = [2, 3, 0, 1]
ax.legend(
    [handles[i] for i in legend_order],
    [labels[i] for i in legend_order],
    loc="upper center",
    bbox_to_anchor=(0.5, -0.15),
    ncol=2,
    fontsize=10
)
ax.grid(axis='y')
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
ax.yaxis.set_major_formatter(CommCompGraph.mega2giga_formatter)
ax.set_axisbelow(True)
ax.set(title=plot_title, xlabel="Number of computing Cores", ylabel="Memory Bandwidth (GB/s)")
plt.subplots_adjust(bottom=0.22)
plt.savefig(f"{comp_kernel}/comp_{numa_comp}_comm_{numa_comm}_stacked{picture_file_suffix}", bbox_inches="tight")
plt.close(fig_stack)

error = compute_error(
    [parser.comp_with_comm_results[op][x][metric]['avg']+parser.comm_bw_with_comp_results[x]['med'] for x in real_x_values[error_total_model_x_start:]],
    total_model[error_total_model_x_start:]
)
print(f"Error on total prediction: {error*100:3.2f}%")




print("#### Plot 3 (total is linear after contention, with an inflection point)")

picture_file_suffix = "_model_v3.png"

fig_stack = plt.figure()
ax = fig_stack.gca()
plt.stackplot(
    real_x_values,
    [[parser.comp_with_comm_results[op][x][metric]['avg'] for x in real_x_values],
    [parser.comm_bw_with_comp_results[x]['med'] for x in real_x_values]],
    labels=["Computations (with communications)", "Communications (with computations)"],
    colors=[color_comm, color_comp],
)
plt.plot(
    real_x_values,
    [parser.comp_alone_results[op][x][metric]['avg'] for x in real_x_values],
    color=color_comp_alone,
    label="Computations (alone)"
)
plt.plot(
    real_x_values,
    model.total_model,
    "--",
    color=color_total,
    label="Bus capacity - model LINEAR2"
)
handles, labels = ax.get_legend_handles_labels()
legend_order = [2, 3, 0, 1]
ax.legend(
    [handles[i] for i in legend_order],
    [labels[i] for i in legend_order],
    loc="upper center",
    bbox_to_anchor=(0.5, -0.15),
    ncol=2,
    fontsize=10
)
ax.grid(axis='y')
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
ax.yaxis.set_major_formatter(CommCompGraph.mega2giga_formatter)
ax.set_axisbelow(True)
ax.set(title=plot_title, xlabel="Number of computing Cores", ylabel="Memory Bandwidth (GB/s)")
plt.subplots_adjust(bottom=0.22)
plt.savefig(f"{comp_kernel}/comp_{numa_comp}_comm_{numa_comm}_stacked{picture_file_suffix}", bbox_inches="tight")
plt.close(fig_stack)

error = compute_error(
    [parser.comp_with_comm_results[op][x][metric]['avg']+parser.comm_bw_with_comp_results[x]['med'] for x in real_x_values[error_total_model_x_start:]],
    model.total_model[error_total_model_x_start:]
)
print(f"Error on total prediction: {error*100:3.2f}%")



print("#### Plot 4 (communications take everything they need)")

comm_with_comp_model = []
comp_with_comm_model = []
for i in range(len(model.x_model)):
    comm_with_comp_model.append(model.comm_alone_bw)
    comp_with_comm_model.append(min(model.total_model[i]-model.comm_with_comp_model[i], model.x_model[i]*model.comp_mem_req))

plot_repartition("_model_v4.png", comm_with_comp_model, comp_with_comm_model, "COMM1")



print("#### Plot 5 (communications are impacted by a factor alpha)")

comm_with_comp_model = []
comp_with_comm_model = []
for i in range(len(model.x_model)):
    if model.comp_mem_req*model.x_model[i] + model.impacted_network_ratio*model.comm_alone_bw < model.total_model[i]:
        comp_with_comm_model.append(model.comp_mem_req*model.x_model[i])
        comm_with_comp_model.append(min(model.comm_alone_bw, model.total_model[i]-comp_with_comm_model[-1]))
    else:
        comm_with_comp_model.append(model.impacted_network_ratio*model.comm_alone_bw)
        comp_with_comm_model.append(model.total_model[i]-comm_with_comp_model[i])

plot_repartition("_model_v5.png", comm_with_comp_model, comp_with_comm_model, "COMM2")


print("#### Plot 6 (the full model)")

plot_repartition("_model_v6.png", model.comm_with_comp_model, model.comp_with_comm_model, "FINAL")

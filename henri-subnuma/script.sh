#!/bin/bash

function execution()
{
    comp_numa_node=$1
    shift
    comm_numa_node=$1
    shift

    echo "** Running with comp_numa_node=$comp_numa_node and comm_numa_node=$comm_numa_node **"

    date

    echo "--one_computing_rank --only_pong --no_alone=comm --bench=bandwidth $* --bind_memory_comp=$comp_numa_node --bind_memory_comm=$comm_numa_node"
    for i in $(seq 1 35)
    do
        echo $i
        mpirun -n 2 -nodelist henri0,henri1 --timeout 600 \
            -DOMP_NUM_THREADS=$i -DOMP_PROC_BIND=true -DOMP_PLACES=cores \
            hwloc-bind --cpubind core:0-$((i-1)) \
            ~/memory-bottleneck/build/src/bench_openmp \
            --one_computing_rank --per_thread --only_pong --no_alone=comm --bench=bandwidth $* --throughput --bind_memory_comp=$comp_numa_node --bind_memory_comm=$comm_numa_node \
            > comp_${comp_numa_node}_comm_${comm_numa_node}_$((i))_threads.out
    done

    date
}

function run_program()
{
    echo "*** Running $1 ***"

    if [ ! -d $1 ]
    then
        mkdir $1
    fi
    cd $1
    shift

    execution 0 0 $*
    execution 2 2 $*
    execution 0 1 $*
    execution 0 2 $*
    execution 0 3 $*
    execution 1 0 $*
    execution 1 1 $*
    execution 1 2 $*
    execution 1 3 $*
    execution 2 0 $*
    execution 2 1 $*
    execution 2 3 $*
    execution 3 0 $*
    execution 3 1 $*
    execution 3 2 $*
    execution 3 3 $*

    cd ..
}

run_program memset_nt --compute_bench=memset --nt

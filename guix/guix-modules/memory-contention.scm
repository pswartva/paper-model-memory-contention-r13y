(define-module (memory-contention)
    #:use-module (gnu packages autotools)
    #:use-module (gnu packages mpi)
    #:use-module (gnu packages pkg-config)
    #:use-module (guix build-system gnu)
    #:use-module (guix git-download)
    #:use-module (guix licenses)
    #:use-module (guix packages)
    #:use-module (gnu packages python)
    #:use-module (gnu packages python-build)
    #:use-module (gnu packages python-science)
    #:use-module (gnu packages python-xyz))

(define-public memory-contention
    (package
        (name "memory-contention")
        (version "0.1")
        (home-page "https://gitlab.inria.fr/pswartva/memory-contention")
        (source
            (origin
                (method git-fetch)
                (uri
                    (git-reference
                        (url "https://gitlab.inria.fr/pswartva/memory-contention.git")
                        (commit "9d4161b223960347c5d9cbddb345820ce61289fc")))
                (file-name (string-append name "-checkout"))
                (sha256 (base32 "1x8xaxfzj8q0f82m25hvgnkadxrcm21h91j1b4wsk8ba0kqz1rch"))))
        (build-system gnu-build-system)
        (arguments
          '(#:out-of-source? #t
            #:phases (modify-phases %standard-phases
                                    (add-before 'install 'pre-install
                                                (lambda _ (setenv "SOURCE_DATE_EPOCH" "315532800") #t)))))
        (properties '((tunable? . #true)))
        (native-inputs (list autoconf pkg-config automake python-wrapper python-pip python-setuptools python-wheel))
        (propagated-inputs
            (list `(,hwloc "lib")
                  hwloc
                  openmpi
                  python-pandas
                  python-scipy
                  python-numpy
                  python-matplotlib
                  python-pillow))
        (synopsis "Set of benchmarks and tools to measure interferences between computations and communications")
        (description
          "memory-contention is composed of a set of benchmarks to measure
          the possible interferences between computations executed by CPU cores
          and MPI communications. It contains also Python scripts to easily
          plot and analyze the results of these benchmarks.")
        (license gpl3)))

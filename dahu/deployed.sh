#!/bin/bash

custom_file_nodes=$1

working_dir="$( cd "$(dirname "$0")" ; pwd -P )"

cd $working_dir

. /etc/profile.d/guix.sh

export GUIX_PACKAGE_PATH=../guix/guix-modules:${GUIX_PACKAGE_PATH}
guix_channels_file=../guix/guix-channels.scm


node_nb_cores=$(guix time-machine --channels=$guix_channels_file -- shell --pure --check hwloc -- hwloc-calc all -N core)
node_nb_numa=$(guix time-machine --channels=$guix_channels_file -- shell --pure --check hwloc -- hwloc-calc all -N node)

echo "**** Found $node_nb_numa NUMA nodes and $node_nb_cores cores per node ****"


guix time-machine --channels=$guix_channels_file -- shell hwloc -- lstopo --of xml > topology.xml

date
guix time-machine --channels=$guix_channels_file -- \
	shell --pure --preserve=^OAR --preserve=^SSH \
	coreutils memory-contention openssh which sed tar inetutils procps gzip grep gawk \
	--with-input=openmpi=nmad-mini --tune -- \
	./exp.sh $node_nb_cores $node_nb_numa $custom_file_nodes
date

#!/bin/bash

node_nb_cores=$1
node_nb_numa=$2
custom_file_nodes=$3

real_nb_cores=$((node_nb_cores-1))

mkdir memset_nt
cd memset_nt

date
for i_numa_comp in $(seq $node_nb_numa)
do
        real_i_numa_comp=$((i_numa_comp-1))
        for i_numa_comm in $(seq $node_nb_numa)
        do
                real_i_numa_comm=$((i_numa_comm-1))
                echo "** Running with comp_numa_node=${real_i_numa_comp} and comm_numa_node=${real_i_numa_comm} **"
                date
                for i in $(seq $real_nb_cores)
                do
                        echo $i
                        mpirun -timeout 600 -machinefile $custom_file_nodes -DPATH=$PATH -DLD_LIBRARY_PATH=$LD_LIBRARY_PATH -DNMAD_DISPLAY_DRIVERS=1 \
                                -DOMP_NUM_THREADS=$i -DOMP_PROC_BIND=true -DOMP_PLACES=cores \
                                hwloc-bind --cpubind core:0-$((i-1)) \
                                $HOME/dev/memory-contention/build/src/bench_openmp \
                                --one_computing_rank --per_thread --only_pong --bench=bandwidth \
                                --bind_memory_comp=${real_i_numa_comp} --bind_memory_comm=${real_i_numa_comm} \
                                --compute_bench=memset --nt --throughput \
                                > comp_${real_i_numa_comp}_comm_${real_i_numa_comm}_${i}_threads.out
                done
        done
done
date

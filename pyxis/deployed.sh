#!/bin/bash

custom_file_nodes=$1

working_dir="$( cd "$(dirname "$0")" ; pwd -P )"

cd $working_dir


source /etc/profile.d/lmod.sh
module use /home/pswartvagher/modulefiles

export NMAD_IBVERBS_RCACHE=1
export NMAD_STRATEGY=prio

module load aarch64/hwloc-dev/hwloc
module load aarch64/pm2/pm2-madmpi
which mpirun

node_nb_cores=$(hwloc-calc all -N core)
node_nb_numa=$(hwloc-calc all -N node)


echo "**** Found $node_nb_numa NUMA nodes and $node_nb_cores cores per node ****"

lstopo --of xml > topology.xml

date
./exp.sh $node_nb_cores $node_nb_numa $custom_file_nodes
date

module unload aarch64/pm2/pm2-madmpi
module unload aarch64/hwloc-dev/hwloc

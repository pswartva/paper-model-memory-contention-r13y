#!/bin/bash

custom_file_nodes=$HOME/oar_nodes_$OAR_JOBID

uniq $OAR_FILE_NODES > $custom_file_nodes

kadeploy3 -e debian10-arm64-std -f $OAR_FILE_NODES -k

ssh $(head -n 1 $OAR_FILE_NODES) $PWD/deployed.sh $custom_file_nodes

rm $custom_file_nodes

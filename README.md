# Paper Model Memory Contention Reproducibility

[![SWH](https://archive.softwareheritage.org/badge/origin/https://gitlab.inria.fr/pswartva/paper-model-memory-contention-r13y/)](https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.inria.fr/pswartva/paper-model-memory-contention-r13y)
[![SWH](https://archive.softwareheritage.org/badge/swh:1:dir:d10e4aa03ddd534c0e26c86db8c365ffece1ae36/)](https://archive.softwareheritage.org/swh:1:dir:d10e4aa03ddd534c0e26c86db8c365ffece1ae36;origin=https://gitlab.inria.fr/pswartva/paper-model-memory-contention-r13y;visit=swh:1:snp:306f7c10cf69a5860587e5aad62b76070b798ecd;anchor=swh:1:rev:c4a24c2d0569882b2e48b6bb4fc3180dea907b8e)

This repository contains the material to reproduce the experiments presented in
the paper *Modeling Memory Contention between Communications and Computations
in Distributed HPC Systems*.


## Requirements

### Hardware

Regarding the hardware, you need two networked HPC nodes to reproduce presented
experiments.

### Software

Benchmarks executed for the paper, as well as the scripts to generate the
plots, come from the *memory-contention* benchmarking suite, located in its own
[repository](https://gitlab.inria.fr/pswartva/memory-contention). The used
commit was `e2d788f5718386c818f0aa07e826fd9e8c6b4870`.



## Organisation

The large set of clusters used to evaluate our model has different software
environment available, resulting in several scripts doing the same things
(running benchmarks, instanciating the model and evaluating it) within
different software stacks:
- `billy`, `henri` and `henri-subnuma`: lab machines, manually compiled, no job
  scheduler;
- `bora` and `diablo`: [PlaFRIM](https://www.plafrim.fr/), compiled with Guix,
  SLURM;
- `dahu` and `grvingt`: [Grid5000](https://www.grid5000.fr), compiled with
  Guix, OAR;
- `pyxis`: [Grid5000](https://www.grid5000.fr), manually compiled (Guix is not
  available for its processor architecture on Grid5000), OAR;
- `occigen`: [Occigen](https://www.cines.fr/calcul/materiels/occigen/) (the
  Broadwell partition was used), manually compiled, SLURM.

When available, [Guix](http://guix.gnu.org/) is used to ensure easy
reproducibility of the experiments, with the
[Guix-HPC](https://gitlab.inria.fr/guix-hpc/guix-hpc/) channel. Guix revisions
are pinned in the file `guix/guix-channels.scm`, and the package
`memory-contention` is defined in `guix/guix-modules/memory-contention.scm`.

We use [NewMadeleine](https://pm2.gitlabpages.inria.fr/NewMadeleine) as
communication engine.

To gather informations about machine topology and bind threads and memory,
[hwloc](https://www.open-mpi.org/projects/hwloc/) is used.

Result analyzis and plots were made with Python scripts of
[memory-contention](https://gitlab.inria.fr/pswartva/memory-contention), with
the [Maplotlib](https://matplotlib.org/) library.

Scripts and specific files are in folders of each machine.



### Manual installation

Hwloc is manually built:
- on `pyxis` from the commit `f95e4b8ecb38819ad067b9b49053d4f7a66771f3`;
- on `occigen` from release 2.5.0.

NewMadeleine is built with the following commands:
```bash
git clone https://gitlab.inria.fr/pm2/pm2.git
cd pm2
git checkout 5eaab4cc1e8d70061db813a598af227efba52dc9
cd scripts
./pm2-build-packages ./pukabi+madmpi-mini.conf --prefix=<installation prefix>
# Set environment variables as indicated at the end of installation
```

memory-contention is built with the following commands:
```bash
git clone https://gitlab.inria.fr/pswartva/memory-contention.git
cd memory-contention
git checkout e2d788f5718386c818f0aa07e826fd9e8c6b4870
./autogen.sh
mkdir build
cd build
../configure
make
```

On `pyxis` and `occigen`, module files are used to easily set required
environment variables.


### Guix installation

When Guix is used, simply follow instructions to launch the benchmarks, Guix
will take care of compiling everything as required.



## Experiment workflow

### Gather machine topology information

We use `lstopo`, from Hwloc:
```bash
lstopo --of xml > topology.xml
```
On the majority of machines, it is done by the script to launch benchmarks.

### Launch benchmarks

For each machine, run the entry-point script `script.sh`. You can find file
system paths hardcoded with my username (to be changed by yours), with the
following command:
```bash
grep -Irn pswartvagher
```

### Instanciate model and plot results

You can instanciate model and plot results, by moving to the folder containing
the topology file and the `memset_nt` folder.

With Guix, run:
```bash
guix time-machine --channels=../guix/guix-channels.scm -- \
    shell -L ../guix/guix-modules/ --check --pure \
    coreutils bash memory-contention \
    --with-input=openmpi=nmad-mini -- \
    plot_model.py --combine memset_nt topology.xml | tee memset_nt/model.out
```

Without Guix, run:
```bash
python3 <folder containing memory-contention sources>/memory-contention/python/plot_model.py --combine memset_nt topology.xml | tee memset_nt/model.out
```

Stacked plots can be generated with the following options:
```bash
python3 <folder containing memory-contention sources>/memory-contention/python/plot_model.py --no-model memset_nt topology.xml > /dev/null
```

To generate the plots used to explain the model construction in the journal
paper, use the following command:
```bash
cd henri-subnuma
python3 plot_progressive_model.py
```
